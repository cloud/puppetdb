FROM gitlab-registry.cern.ch/cloud/puppetbase:latest
MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

RUN yum install -y \
    puppetdb && \
    yum clean all

ADD database.ini /etc/puppetlabs/puppetdb/conf.d

CMD [ "sh", "-c", "sleep 5; /opt/puppetlabs/bin/puppet agent --test --debug || true; /opt/puppetlabs/bin/puppetdb ssl-setup || true; /opt/puppetlabs/bin/puppetdb foreground" ]
